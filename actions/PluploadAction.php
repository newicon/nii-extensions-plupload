<?php

class PluploadAction extends CAction
{

	public function onUpload($event)
	{
		$this->raiseEvent('onUpload',$event);
	}

	public function onDelete($event)
	{
		$this->raiseEvent('onDelete',$event);
	}
/**
	 * process the filemanager id
	 * @return int filemanager id 
	 */
	public function run()
	{
		// If this is a DELETE request, delete the file
		if( Yii::app()->request->isDeleteRequest )
		{
			parse_str(file_get_contents("php://input"),$delete_vars);
			$file_id = $delete_vars['file_id'];

			echo CJSON::encode( array('result' => NFileManager::get()->deleteFile($file_id)) );

			// Raise an event for controllers
			$data = array_merge( $delete_vars, $_GET);
			$event = new CEvent($this,$data);
			$this->onDelete($event);
			return true;
		}

		// Else process the file contents
		$fileContents 	= $this->processUpload($targetDir, $fileName, $orginalName);
		$file_id 		= NFileManager::get()->addFile($fileName, $fileContents);

		$data = array(
			'file'			=> NFileManager::get()->getFile($file_id),
            'file_id'		=> $file_id,
            'url'			=> NFileManager::get()->getUrl($file_id),
        );
		echo CJSON::encode($data);

        // Raise an event for controllers to get
        $event = new CEvent($this, $data);
        $this->onUpload($event);
		return true;
	}
	
	/**
	 * Process the upload
	 * @param string $targetDir byref 
	 * @param string $fileName
	 * @param string $orginalName
	 * @return binary file contents 
	 */
	private function processUpload(&$targetDir, &$fileName, &$orginalName)
	{
		// HTTP headers for no cache etc
		header('Content-type: text/plain; charset=UTF-8');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		// Settings
		$targetDir = Yii::app()->getRuntimePath();
		$cleanupTargetDir = false; // Remove old files
		$maxFileAge = 60 * 60; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		//sleep(5);
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
		$chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
		$orginalName = $fileName;
		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '', $fileName);

		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);

			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;

			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}

		// Create target dir
		if (!file_exists($targetDir))
			@mkdir($targetDir);

		// Remove old temp files
		if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$filePath = $targetDir . DIRECTORY_SEPARATOR . $file;

				// Remove temp files if they are older than the max age
				if (preg_match('/\\.tmp$/', $file) && (filemtime($filePath) < time() - $maxFileAge))
					@unlink($filePath);
			}

			closedir($dir);
		} else {
			Yii::log('Failed to open temp directory.', 'error', 'nii.widgets.plupload');
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
		}

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];

		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");

					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else {
						Yii::log('Failed to open input stream.', 'error', 'nii.widgets.plupload');
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					}
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else {
					Yii::log('Failed to open output stream.', 'error', 'nii.widgets.plupload');
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
				}
			} else {
				Yii::log('Failed to move uploaded file.', 'error', 'nii.widgets.plupload');
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}
		} else {
			// Open temp file
			$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");

				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else {
					Yii::log('Failed to open input stream.', 'error', 'nii.widgets.plupload');
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
				fclose($in);
				fclose($out);
			} else {
				Yii::log('Failed to open output stream.', 'error','nii.widgets.plupload');
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			}
		}
		
		$file = $targetDir . DIRECTORY_SEPARATOR . $fileName;
		$fileContents = file_get_contents($file);
		
		// remove the tempory file
		unlink($file);
		
		return $fileContents;
	}
}