Plupload Nii Extension
======================

A version of the Plupload javascript API packaged for a Nii installation. Comprises 3 main components:

* The Plupload Javascript source files
* A CApplicationComponent "Plupload" to register the source files
* A CAction implementation "PluploadAction" which can be registered in controllers to recieve an upload.

Component
------------------------------

This component serves one real purpose - to register the plupload javascript source files.
How you wish to invoke the registerScripts function is up to you.

```php
Yii::app()->setComponents('plupload' => array('class' => 'niiextensions.plupload.components.Plupload'));
Yii::app()->plupload->registerScripts();
```

Actions
-------

The extension comes with one action, PluploadAction.php. This action provides a basic mechanism for accepting and storing
a file upload coming from a Plupload request.

The action relies on the NFileManager to store and manage files.

The action should be registered by the controller e.g.

```php
/// MyController.php
class MyController extends CController
{
	.........

        public function actions()
        {
                return array(
                        'upload' => array(
                                'class'         => 'niiextensions.plupload.actions.PluploadAction',
                        ),
                );
        }
}
```

The PluploadAction also fires an event once an upload has been stored. Registering for the event can be achieved in
the controller like this:

```php
// MyController.php
class MyController extends CController
{
	.........

    /* uploadHandlerMethod

       Imagine somewhere in your view or javascript you have the following:

       // begin js
       // Initialize the plupload Uploader
        var uploader = new plupload.Uploader({
                runtimes: 'html5,flash',
                url: 'mycontroller/upload?model_id=1&model_class=MyModel&attribute=image_id'
        });
        uploader.init();
        // end js
    */
	public function uploadHandlerMethod($event)
	{
		$params = $event->params;
		$model_id 		= Yii::app()->getRequest()->getQuery('model_id',null);
		$model_class 	= Yii::app()->getRequest()->getQuery('model_class',null);
		$attribute 		= Yii::app()->getRequest()->getQuery('attribute',null);

		$model = $model::model()->findByPk($model_id);
		NFileManager::get()->deleteFile($mode->{$attribute});

		$rep->{$attribute} = $params['file_id'];
		$rep->save();
	}

    public function actions()
    {
            return array(
                        'upload' => array(
                                'class'         => 'niiextensions.plupload.actions.PluploadAction',
                                'onUpload'      => array($this,'uploadHandlerMethod')
                        ),
                );
    }
}
```

In the event callback, $event->params will contain the following:

* file_id - the ID of the NFile object representing the stored file
* file - the NFile object for the file
* url - the URL of the file

