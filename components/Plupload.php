<?php
class Plupload extends CApplicationComponent{

    const ASSETS_DIR_NAME       = 'assets';
    const NPLUPLOAD_FILE_NAME   = 'js/nplupload.js';
    const PLUPLOAD_FILE_NAME    = 'plupload/js/plupload.full.js';
    const JQUERYQUEUE_FILE_NAME = 'plupload/js/jquery.plupload.queue/jquery.plupload.queue.js';
    const BROWSER_PLUS_URL      = 'http://bp.yahooapis.com/2.4.21/browserplus-min.js';
    const FLASH_FILE_NAME       = 'plupload/js/plupload.flash.swf';
    const SILVERLIGHT_FILE_NAME = 'plupload/js/plupload.silverlight.xap';
    const DEFAULT_RUNTIMES      = 'html5,flash,silverlight,html4';
    const PUPLOAD_CSS_PATH      = 'plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css';

    protected $_assetsUrl;

    public function registerScripts()
    {
        $assetsUrl = $this->getAssetsUrl();
        
        //Yii::app()->clientScript->registerScriptFile(self::BROWSER_PLUS_URL);

        $pluploadPath = $assetsUrl . "/" . self::PLUPLOAD_FILE_NAME;
        Yii::app()->clientScript->registerScriptFile($pluploadPath);

        $jQueryQueuePath = $assetsUrl . "/" . self::JQUERYQUEUE_FILE_NAME;
        Yii::app()->clientScript->registerScriptFile($jQueryQueuePath);

        $cssPath = $assetsUrl . "/" . self::PUPLOAD_CSS_PATH;
        Yii::app()->clientScript->registerCssFile($cssPath);   
    }

    public function getAssetsUrl()
    {
        if($this->_assetsUrl !== null)
            return $this->_assetsUrl;

        $assetsPath = Yii::getPathOfAlias('niiextensions.plupload.assets');
        $assetsUrl = Yii::app()->assetManager->publish($assetsPath);
        return $this->_assetsUrl = $assetsUrl;
    }
}
?>
